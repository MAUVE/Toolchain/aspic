#####
# Copyright 2017-2018 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from aspic import *
from aspic.utils import draw

def test_operator(N, name=""):
    for e in N.status(entry):
        p = N.place(e)
        p.add([sdot])
    draw(N, "operator-{}.pdf".format(name))

    def draw_sg(st, sg, attr):
        attr["label"] = "{}".format(sg[st])

    g = StateGraph(N)
    g.build()
    g.draw("operator-{}-graph.pdf".format(name), node_attr=draw_sg)


h = DefaultHandler("h")
A = handle(SkillPetriNet("A", locks={'L': 1}), h.copy())
B = handle(SkillPetriNet("B"), h.copy())
C = handle(SkillPetriNet("C", locks={'L': 1}), h.copy())

test_operator(A+B, "choice")
test_operator(sequence(A, B), "seq")
test_operator(concurrency(A, B), "concur")
test_operator(ite(A, B, C), "ite")
test_operator(retry(A), "retry")
test_operator(negation(A), "neg")
test_operator(race(A, B), "race")
