#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from aspic import *
from aspic.ros import *
#import rospy
#from actionlib_tutorials.msg import FibonacciAction, FibonacciGoal
#rospy.init_node("aspic")

# Exercice 1: fibonacci client
Sfib = SkillPetriNet("Sfib", inputs=['n'], outputs=['sequence', 'value'])
draw(Sfib, "Sfib.pdf")
sys.exit(0)

fib_client = RosActionlibClient('fibonacci', FibonacciAction,
                        goal_fmt=lambda t: FibonacciGoal(order=t[0]),
                        result_fmt=lambda r: hdict({'sequence': r.sequence, 'value': r.sequence[-1]}))
N_fib = handle(S_fib, RosActionlibHandler(fib_client))

# Exercice 2: test handler + sequence
S_test = SkillPetriNet("Test", inputs=['value', 'K'])
H_test = TestHandler("Test", fun=lambda x: x[0] > x[1])
N_test = handle(S_test, H_test)
N = sequence(N_fib, N_test)

# Exercice 3: increment 'n' if fdot
S_inc = SkillPetriNet("Inc", inputs=['n'], outputs=['n'])
H_inc = AssignHandler("Inc", fun=lambda x: hdict({'n': x[0] + 1}))
N_inc = handle(S_inc, H_inc)
N = sequence(negation(N), N_inc)

# Exercice 4: return success if test
N = negation(N)

# Exercice 5: loop until test
N = retry(N)

initialize(N, {
    'n': 1,
    'value': -1,
    'sequence': 0,
    'K': 4,
    })

draw(N, "ros-fibonacci-pn.pdf")
rate = rospy.Rate(1)
fib_client.wait_for_server()

while not rospy.is_shutdown():
    step(N, verbose=True, once=True)
    draw(N, "ros-fibonacci-pn.pdf")
    if has_succeeded(N):
        print("{} has succeeded".format(N))
        break
    elif has_failed(N):
        print("{} has failed".format(N))
        break
    rate.sleep()
