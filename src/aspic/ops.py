#####
# Copyright 2017-2018 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import *
from snakes.plugins.ops import _glue
from .tokens import sdot, fdot, tControlFlowToken

def sequence (N_1, N_2) :
    """ Sequential composition
    Enforces execution of N_1 followed by N_2, except if N_1 terminates with an error, in which case execution of N_2 is skipped

    :param N_1: first net
    :type N_1: PetriNet
    :param N_2: second net
    :type N_2: PetriNet
    :return: N_sequence[N_1, N_2]
    :rtype: PetriNet
    """
    x = N_1.status(exit)[0]
    e = N_2.status(entry)[0]
    result = N_1 & N_2
    new = "[%s&%s]" % (x, e)
    result.add_transition(Transition("t_throw"))
    result.add_input(new, "t_throw", Value(fdot))
    x = result.status(exit)[0]
    result.add_output(x, "t_throw", Value(fdot))
    return result

def concurrency (N_1, N_2) :
    """ Concurrent composition
    Executes N_1 and N_2 concurrently; when both are terminated, the whole compoisition results in a success if both nets succeeded.

    :param N_1: first net
    :type N_1: PetriNet
    :param N_2: second net
    :type N_2: PetriNet
    :return: N_concur[N_1, N_2]
    :rtype: PetriNet
    """
    result = _glue("|", N_1, N_2)
    # New entry/start
    entries = result.status(entry)
    result.add_place(Place("p_e", check=tControlFlowToken, status=entry))
    result.add_transition(Transition("t_start"))
    result.add_input("p_e", "t_start", Value(sdot))
    for e in entries:
        result.set_status(e, internal)
        result.add_output(e, "t_start", Value(sdot))
    # New exit
    exits = result.status(exit)
    result.add_place(Place("p_x", check=tControlFlowToken, status=exit))
    # Success transition
    result.add_transition(Transition("t_success"))
    for e in exits:
        result.add_input(e, "t_success", Value(sdot))
        result.set_status(e, internal)
    result.add_output("p_x", "t_success", Value(sdot))
    # Failure transition
    result.globals['fdot'] = fdot
    result.add_transition(Transition("t_failure", Expression("fdot in [u, v]")))
    e_1, e_2 = exits
    result.add_input(e_1, "t_failure", Variable('u'))
    result.add_input(e_2, "t_failure", Variable('v'))
    result.add_output("p_x", "t_failure", Value(fdot))
    return result

def ite(N_1, N_2, N_3):
    """ If-then-else
    Executies first N_1 , then N_2 if N_1 succeeds, otherwise N_3

    :param N_1: first net
    :type N_1: PetriNet
    :param N_2: net executed if N_1 succeeds
    :type N_2: PetriNet
    :param N_3: net executed if N_1 fails
    :type N_3: PetriNet
    :return: If N_1 then N_2 else N_3
    :rtype: PetriNet
    """
    result = N_1 & N_2
    result = _glue("E", result, N_3)
    # Get new labels
    e_3 = N_3.status(entry)[0]
    e_2 = N_2.status(entry)[0]
    x_1 = N_1.status(exit)[0]
    new_i = "[[%s&%s]E]" % (x_1, e_2)
    new_e = "[E%s]" % (e_3)
    # Add catch transition
    result.add_transition(Transition("t_catch"))
    result.add_input(new_i, "t_catch", Value(fdot))
    result.add_output(new_e, "t_catch", Value(sdot))
    result.set_status(new_e, internal)
    # Merge exits
    xs = result.status(exit)
    result.merge_places("p_x", xs)
    for x in xs: result.remove_place(x)
    return result

def retry(N):
    """ Retry
    Retry execution of N until success

    :param N: Petri net
    :type N: PetriNet
    :return: N_retry[N]
    :rtype: PetriNet
    """
    result = N.copy()
    e = result.status(entry)[0]
    # New exit place
    x = result.status(exit)[0]
    xR = "{}_R".format(x)
    result.set_status(x, internal)
    result.add_place(Place(xR, check=tControlFlowToken, status=exit))
    # Terminal transition
    t = "{}_end".format(xR)
    result.add_transition(Transition(t))
    result.add_input(x, t, Value(sdot))
    result.add_output(xR, t, Value(sdot))
    # Retry transition
    t = "{}_retry".format(xR)
    result.add_transition(Transition(t))
    result.add_input(xR, t, Value(fdot))
    result.add_output(e, t, Value(sdot))
    return result

def negation(N):
    """ Negation
    Inverts the termination status of N

    :param N: Petri net
    :type N: PetriNet
    :return: N_not[N]
    :rtype: PetriNet
    """
    result = N.copy()
    # New exit place
    x = result.status(exit)[0]
    xN = "{}_N".format(x)
    result.set_status(x, internal)
    result.add_place(Place(xN, check=tControlFlowToken, status=exit))
    # Transition if success
    t = "{}_t_s2f".format(xN)
    result.add_transition(Transition(t))
    result.add_input(x, t, Value(sdot))
    result.add_output(xN, t, Value(fdot))
    # Transition if success
    t = "{}_t_f2s".format(xN)
    result.add_transition(Transition(t))
    result.add_input(x, t, Value(fdot))
    result.add_output(xN, t, Value(sdot))
    return result

def race(N_1, N_2):
    """ Race composition
    Executes both N_1 and N_2; if N_1 terminates first, terminates the whole composition; the same if N_2 terminates first

    :param N_1: first net
    :type N_1: PetriNet
    :param N_2: second net
    :type N_2: PetriNet
    :return: N_race[N_1, N_2]
    :rtype: PetriNet
    """
    result = _glue('X', N_1, N_2)
    # New entry/start
    entries = result.status(entry)
    result.add_place(Place("p_e", check=tControlFlowToken, status=entry))
    result.add_transition(Transition("t_start"))
    result.add_input("p_e", "t_start", Value(sdot))
    for e in entries:
        result.set_status(e, internal)
        result.add_output(e, "t_start", Value(sdot))
    # Internal place
    result.add_place(Place("p_i", check=tControlFlowToken, status=internal))
    result.add_output("p_i", "t_start", Value(sdot))
    # Exits
    x_1 = N_1.status(exit)[0]
    x_2 = N_2.status(exit)[0]
    new_x_1 = "[%sX]" % (x_1)
    new_x_2 = "[X%s]" % (x_2)
    result.add_place(Place("p_x", check=tControlFlowToken, status=exit))
    result.set_status(new_x_1, internal)
    result.set_status(new_x_2, internal)
    # If N_1 first
    result.add_transition(Transition("t_first_1"))
    result.add_input("p_i", "t_first_1", Value(sdot))
    result.add_input(new_x_1, "t_first_1", Variable('x'))
    result.add_output("p_x", "t_first_1", Variable('x'))
    result.add_output("p_i", "t_first_1", Value(fdot))
    # If N_2 first
    result.add_transition(Transition("t_first_2"))
    result.add_input("p_i", "t_first_2", Value(sdot))
    result.add_input(new_x_2, "t_first_2", Variable('x'))
    result.add_output("p_x", "t_first_2", Variable('x'))
    result.add_output("p_i", "t_first_2", Value(fdot))
    # If N_1 last
    result.add_transition(Transition("t_last_1"))
    result.add_input("p_i", "t_last_1", Value(fdot))
    result.add_input(new_x_1, "t_last_1", Variable('x'))
    # If N_2 last
    result.add_transition(Transition("t_last_2"))
    result.add_input("p_i", "t_last_2", Value(fdot))
    result.add_input(new_x_2, "t_last_2", Variable('x'))
    return result
