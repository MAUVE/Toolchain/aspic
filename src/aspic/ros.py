#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import *
from .tokens import SkillStatus, private
from .ros_transition import RosClientTransition

import rospy
import actionlib

class RosActionlibClient(actionlib.SimpleActionClient):
    """ Wrapper class for ROS Actionlib clients that provides helper functions
    for corresponding handlers.
    """
    def __init__(self, action, ActionSpec, goal_fmt=lambda x: x, result_fmt=lambda x: x):
        """ Constructor
        :param action: server name
        :type action: str
        :param ActionSpec: ROS action type
        :type ActionSpec: a type
        :param goal_fmt: function to format the action goal from arguments passed from the SkPN
        :type goal_fmt: takes SkPN inputs as parameters and returns an ActionGoal
        :param result_fmt: function to format the results passed to the SkPN from action results
        :type result_fmt: takes an ActionResult as parameter and returns outputs for the SkPN
        """
        self.name = action
        actionlib.SimpleActionClient.__init__(self, action, ActionSpec)
        self.goal_fmt = goal_fmt
        self.result_fmt = result_fmt
        self.result = None

    def call(self, goal):
        """ Triggers the action on the server
        :param goal: the goal to send to the server
        :type goal: SkPN input variables
        :return: this
        """
        actionlib.SimpleActionClient.send_goal(self, self.goal_fmt(goal))
        return self

    def val(self):
        """ Returns the result of the action
        :return: the result gathered from the sever, None otherwise
        :rtype: SkPN outputs dict
        """
        g = actionlib.SimpleActionClient.get_result(self)
        if g is None:
            return g
        else:
            return self.result_fmt(g)

    def st(self):
        """ Get status of the action
        :return: the SkillStatus corresponding to the client status
        :rtype: SkillStatus
        """
        s = self.get_state()
        if s == 3:
            return SkillStatus.stopped.name
        elif s in [2, 4, 5, 8]:
            return SkillStatus.failed.name
        else:
            return SkillStatus.active.name

def RosActionlibHandler(client, **args):
    """ ROS actionlib client handler.
    Connects to the server, then launch the action on activation,
    and gathers action status and result each time the 't_status' transition
    is fired.

    :param client: the ROS actionlib client
    :return: the handler Petri net
    :rtype: PetriNet
    """
    pn = PetriNet(client.name, **args)
    pn.add_place(Place("p_exec", status=private))
    pn.add_place(Place("p_cl", status=private, tokens=[client]))
    pn.add_transition(RosClientTransition("t_activate"))
    pn.add_input("p_exec", "t_activate", Tuple([Value(SkillStatus.pending.name), Variable('arg')]))
    pn.add_input("p_cl", "t_activate", Variable('cl'))
    pn.add_output("p_exec", "t_activate", Tuple([Value(SkillStatus.active.name), Value(())]))
    pn.add_output("p_cl", "t_activate", Expression("cl.call(arg)"))
    pn.add_transition(RosClientTransition("t_status"))
    pn.add_input("p_exec", "t_status", Tuple([Value(SkillStatus.active.name), Variable('arg')]))
    pn.add_input("p_cl", "t_status", Variable('cl'))
    pn.add_output("p_exec", "t_status", Tuple([Expression("cl.st()"), Expression("cl.val()")]))
    pn.add_output("p_cl", "t_status", Variable('cl'))
    return pn
