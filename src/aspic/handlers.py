from .snk import *
from .tokens import SkillStatus, private
from snakes.plugins.ops import _glue

def SimulationHandler(name, f_success=(lambda x: hdict({'out': x})), f_failure=(lambda x: hdict({'out': x})), **args):
    """ Simulation handler.
    This handler can equally fire either success or failure.

    :param name: handler name
    :type name: str
    :param f_success: function that assign the results in case of success
    :type f_success: T(T) where T is type of input
    :param f_failure: function that assign the results in case of failure
    :type f_failure: T(T) where T is type of input
    :param args: supplementary arguments passed to the Petri net constructor
    :return: the handler Petri net
    :rtype: PetriNet
    """
    pn = PetriNet(name, **args)
    fs = "{}_success".format(name)
    ff = "{}_failure".format(name)
    pn.globals[fs] = f_success
    pn.globals[ff] = f_failure
    pn.add_place(Place('p_exec'), status=private)
    pn.add_transition(Transition('t_success'))
    pn.add_transition(Transition('t_failure'))
    pn.add_input('p_exec', 't_success', Tuple([Value(SkillStatus.pending.name), Variable('v')]))
    pn.add_input('p_exec', 't_failure', Tuple([Value(SkillStatus.pending.name), Variable('v')]))
    pn.add_output('p_exec', 't_success', Tuple([Value(SkillStatus.stopped.name), Expression("{}(v)".format(fs))]))
    pn.add_output('p_exec', 't_failure', Tuple([Value(SkillStatus.failed.name), Expression("{}(v)".format(ff))]))
    return pn

def TestHandler(name, fun, **args):
    """ Handler that test the value of inputs.
    This handler succeeds if 'fun' evaluates to true on inputs. Otherwise if fails.

    :param name: handler name
    :type name: str
    :param fun: the test function
    :type fun: bool(T) where T is type of input
    :param args: supplementary arguments passed to the Petri net constructor
    :return: the handler Petri net
    :rtype: PetriNet
    """
    pn = PetriNet(name, **args)
    f = "{}_fun".format(name)
    pn.globals[f] = fun
    pn.add_place(Place('p_exec'), status=private)
    pn.add_transition(Transition('t_test_fun', Expression("{}(v)".format(f))))
    pn.add_transition(Transition('t_test_not_fun', Expression("not {}(v)".format(f))))
    pn.add_input('p_exec', 't_test_fun', Tuple([Value(SkillStatus.pending.name), Variable('v')]))
    pn.add_input('p_exec', 't_test_not_fun', Tuple([Value(SkillStatus.pending.name), Variable('v')]))
    pn.add_output('p_exec', 't_test_fun', Tuple([Value(SkillStatus.stopped.name), Value(True)]))
    pn.add_output('p_exec', 't_test_not_fun', Tuple([Value(SkillStatus.failed.name), Value(False)]))
    return pn

def AssignHandler(name, fun, **args):
    """ Handler that assigns the value of outputs from inputs.
    This handler always succeeds. It returns the evaluation of fun on inputs.

    :param name: handler name
    :type name: str
    :param fun: the assignement function
    :type fun: T(T) where T is type of input
    :param args: supplementary arguments passed to the Petri net constructor
    :return: the handler Petri net
    :rtype: PetriNet
    """
    pn = PetriNet(name, **args)
    f = "{}_fun".format(name)
    pn.globals[f] = fun
    pn.add_place(Place('p_exec'), status=private)
    pn.add_transition(Transition('t_inc'))
    pn.add_input('p_exec', 't_inc', Tuple([Value(SkillStatus.pending.name), Variable('v')]))
    pn.add_output('p_exec', 't_inc', Tuple([Value(SkillStatus.stopped.name), Expression("{}(v)".format(f))]))
    return pn

# an alias for the default handler
DefaultHandler = SimulationHandler

def handle(N, N_h):
    """ Handle operation.
    Build a new Petri net by merging the 'p_exec' places of the Skill PN and the handler.

    :param N: the Skill Petri net
    :type N: PetriNet
    :param N_h: handler Petri net
    :type N_h: PetriNet
    :return: N & N_h
    :rtype: PetriNet
    """
    N.set_status('p_exec', buffer("p_exec"))
    N_h.set_status('p_exec', buffer("p_exec"))
    h = N & N_h
    h.set_status("[p_exec&p_exec]", status=private)
    return h
