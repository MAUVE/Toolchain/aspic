#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import *
from .tokens import SkillStatus, private
from .ros_transition import RosClientTransition
from .utils import step, draw, has_failed, has_succeeded

import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient

class Ros2HandlerContainer(object):
    """ A Hashable container for Ros2 Future objects and GoalHandlers. """
    def __init__(self, h):
        self._handler = h
    @property
    def handler(self):
        return self._handler
    @property
    def accepted(self):
        return self._handler.accepted
    @property
    def status(self):
        return self._handler.status
    def done(self):
        return self._handler.done()
    def get_result_async(self):
        future = self._handler.get_result_async()
        return Ros2HandlerContainer(future)
    def result(self):
        result = self._handler.result()
        return Ros2HandlerContainer(result)
    def value(self):
        return self._handler.result

class Ros2ActionClient:
    """ Wrapper class for ROS2 Action clients that provides helper functions
    for corresponding handlers.
    """
    def __init__(self, node=None, server=None, action=None, client=None,
        goal_fmt=lambda x: x, result_fmt=lambda x: x):
        """ Constructor
        :param action: server name
        :type action: str
        :param ActionSpec: ROS2 action type
        :type ActionSpec: a type
        :param goal_fmt: function to format the action goal from arguments passed from the SkPN
        :type goal_fmt: takes SkPN inputs as parameters and returns an ActionGoal
        :param result_fmt: function to format the results passed to the SkPN from action results
        :type result_fmt: takes an ActionResult as parameter and returns outputs for the SkPN
        """
        if node and server and action:
            self._client = ActionClient(node, action, server)
        elif client:
            self._client = client
        else:
            raise ValueException()

        self.name = action
        self.goal_fmt = goal_fmt
        self.result_fmt = result_fmt
        self.result = None

    def wait_for_server(self):
        return self._client.wait_for_server()

    def send(self, goal):
        """ Triggers the action on the server
        :param goal: the goal to send to the server
        :type goal: SkPN input variables
        :return: a Future on server response
        """
        g, id = self.goal_fmt(goal)
        future = self._client.send_goal_async(g, goal_uuid=id)
        return Ros2HandlerContainer(future)

    def value(self, result):
        """ Returns the result of the action
        :return: the result gathered from the sever, None otherwise
        :rtype: SkPN outputs dict
        """
        return self.result_fmt(result.value())

    def status(self, handle):
        """ Get status of the action
        :return: the SkillStatus corresponding to the goal handle status
        :rtype: SkillStatus
        """
        if handle.status == rclpy.action.client.GoalStatus.STATUS_SUCCEEDED:
            return SkillStatus.stopped.name
        else:
            return SkillStatus.failed.name

def Ros2ActionHandler(client, **args):
    """ ROS2 Action client handler.
    Send goal to the action on activation, and successively gathers
    the server response (acceptance), then the execution status and finally
    the goal result.

    :param client: the ROS actionlib client
    :return: the handler Petri net
    :rtype: PetriNet
    """
    pn = PetriNet('ROS2', **args)
    pn.add_place(Place("p_exec", status=private))
    pn.add_place(Place("p_client", status=private, tokens=[client]))
    pn.add_place(Place("p_sent", status=private))
    pn.add_place(Place("p_handle", status=private))
    pn.add_place(Place("p_response", status=private))
    pn.add_place(Place("p_accepted", status=private))
    pn.add_place(Place("p_result", status=private))
    pn.add_transition(RosClientTransition("t_send"))
    pn.add_input("p_exec", "t_send", Tuple([Value(SkillStatus.pending.name), Variable('arg')]))
    pn.add_input("p_client", "t_send", Variable('client'))
    pn.add_output("p_exec", "t_send", Tuple([Value(SkillStatus.active.name), Value(())]))
    pn.add_output("p_client", "t_send", Variable('client'))
    pn.add_output("p_sent", "t_send", Expression("client.send(arg)"))
    pn.add_transition(RosClientTransition("t_response", guard=Expression('future.done()')))
    pn.add_input("p_sent", "t_response", Variable('future'))
    pn.add_output("p_response", "t_response", Expression("future.result()"))
    pn.add_transition(RosClientTransition("t_rejected", guard=Expression('not (h.accepted)')))
    pn.add_input("p_response", "t_rejected", Variable('h'))
    pn.add_input("p_exec", "t_rejected", Tuple([Variable('st'), Variable('arg')]))
    pn.add_output("p_exec", "t_rejected", Tuple([Value(SkillStatus.failed.name), Value(())]))
    pn.add_transition(RosClientTransition("t_accepted", guard=Expression('h.accepted')))
    pn.add_input("p_response", "t_accepted", Variable('h'))
    pn.add_output("p_handle", "t_accepted", Variable('h'))
    pn.add_output("p_accepted", "t_accepted", Expression("h.get_result_async()"))
    pn.add_transition(RosClientTransition("t_result", guard=Expression('future.done()')))
    pn.add_input("p_accepted", "t_result", Variable('future'))
    pn.add_output("p_result", "t_result", Expression("future.result()"))
    pn.add_transition(RosClientTransition("t_status"))
    pn.add_input("p_exec", "t_status", Tuple([Value(SkillStatus.active.name), Variable('arg')]))
    pn.add_input("p_handle", "t_status", Variable('h'))
    pn.add_input("p_result", "t_status", Variable('result'))
    pn.add_input("p_client", "t_status", Variable('client'))
    pn.add_output("p_client", "t_status", Variable('client'))
    pn.add_output("p_exec", "t_status", Tuple([Expression("client.status(h)"), Expression("client.value(result)")]))
    return pn

class Ros2Player(Node):
    """ A Player node for ROS2. """
    def __init__(self, name, period=1.0, success_cb=rclpy.shutdown, failure_cb=rclpy.shutdown, cb_group=None, verbose=False):
        Node.__init__(self, name)
        self._init_callbacks(period, success_cb, failure_cb, cb_group)
        self._verbose = verbose

    def _init_callbacks(self, period=1.0, success_cb=rclpy.shutdown, failure_cb=rclpy.shutdown, cb_group=None):
        self.timer = self.create_timer(period, self.timer_callback, callback_group=cb_group)
        self.success_cb = success_cb
        self.failure_cb = failure_cb

    @property
    def verbose(self):
        return self._verbose
    @verbose.setter
    def verbose(self, value):
        self._verbose = value

    def timer_callback(self):
        if self.N:
            try:
                step(self.N, verbose=self._verbose, once=True)
                #draw(self.N, self.name+"_pn.pdf")
                if has_succeeded(self.N):
                    print("{} has succeeded".format(self.N))
                    self.success_cb()
                elif has_failed(self.N):
                    print("{} has failed".format(self.N))
                    self.failure_cb()
            except Exception as ex:
                print("Exception {} in ASPiC step: {}".format(type(ex).__name__, ex))

    def run(self, pn):
        self.N = pn
        try:
            rclpy.spin(self)
        except:
            pass
        finally:
            rclpy.shutdown()
