#####
# Copyright 2017-2018 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import *
from .tokens import SkillStatus, sdot, fdot, tControlFlowToken, private

def SkillPetriNet(name, locks=dict(), inputs=[], outputs=[], **args):
    """ Skill Petri Nets construction

    :param name: name of the net
    :type name: str
    :param locks: set of locks
    :type locks: dict with lock names as keys and required number as values
    :param inputs: set of inputs
    :type inputs: list of input names
    :param outputs: set of outputs
    :type outputs: list of output names
    :param args: supplementary arguments passed to PetriNet constructor
    :return: the Skill Petri net
    :rtype: PetriNet
    """
    pn = PetriNet(name, **args)

    # entry, internal and exit places
    pn.add_place(Place('p_e', check=tControlFlowToken, status=entry))
    pn.add_place(Place('p_i', check=tControlFlowToken, status=internal))
    pn.add_place(Place('p_x', check=tControlFlowToken, status=exit))
    # exec place
    pn.add_place(Place('p_exec'), status=private)
    # control-flow places labels
    pn.place('p_i').label(skill=name)
    pn.place('p_exec').label(skill=name)
    # lock places
    for l in locks:
        try:
            pn.add_place(Place(l, check=tBlackToken, status=buffer(l)))
        except snakes.ConstraintError as e:
            pass
    # input places
    for i in inputs:
        try:
            pn.add_place(Place(i, status=buffer(i)))
        except snakes.ConstraintError as e:
            pass
    # output places
    for o in outputs:
        try:
            pn.add_place(Place(o, status=buffer(o)))
        except snakes.ConstraintError as e:
            pass
    # transitions
    pn.add_transition(Transition('t_start'))
    pn.add_transition(Transition('t_stop'))
    pn.add_transition(Transition('t_except'))
    # input arcs
    pn.add_input('p_e', 't_start', Value(sdot))
    pn.add_input('p_i', 't_stop', Value(sdot))
    pn.add_input('p_i', 't_except', Value(sdot))
    pn.add_input('p_exec', 't_stop', Tuple([Value(SkillStatus.stopped.name), Variable('out')]))
    pn.add_input('p_exec', 't_except', Tuple([Value(SkillStatus.failed.name), Variable('out')]))
    for l in locks:
        pn.add_input(l, 't_start', MultiArc([Value(dot)]*locks[l]))
    for i in inputs:
        pn.add_input(i, 't_start', Variable('v_'+i))
    for o in outputs:
        pn.add_input(o, 't_stop', Variable('v_'+o))
        pn.add_input(o, 't_except', Variable('v_'+o))
    # output arcs
    pn.add_output('p_i', 't_start', Value(sdot))
    pn.add_output('p_x', 't_stop', Value(sdot))
    pn.add_output('p_x', 't_except', Value(fdot))
    if inputs:
        pn.add_output('p_exec', 't_start', Tuple([Value(SkillStatus.pending.name), Tuple([Variable("v_"+i) for i in inputs])]))
    else:
        pn.add_output('p_exec', 't_start', Tuple([Value(SkillStatus.pending.name), Value(())]))
    for l in locks:
        pn.add_output(l, 't_stop', MultiArc([Value(dot)]*locks[l]))
        pn.add_output(l, 't_except', MultiArc([Value(dot)]*locks[l]))
    for i in inputs:
        pn.add_output(i, 't_start', Variable('v_'+i))
    for o in outputs:
        pn.add_output(o, 't_stop', Expression("out['"+o+"']"))
        pn.add_output(o, 't_except', Expression("out['"+o+"']"))

    # Hack to generate a _labels dict in nodes
    for n in pn.node():
        n.label()

    return pn
