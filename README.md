# ASPiC

ASPiC is an
acting system based on the modeling of robot’s skills using
a specific control-flow Petri net model. The skills can then be
combined using well-defined operators to build a complete plan
that refines a high-level action. Some properties are guaranteed
by construction, while others can be verified on the resulting
plan model.

## References

There is a [paper](/doc/iros18aspic.pdf) describing ASPiC. If you use ASPiC, please cite this paper:
```
@inproceedings{aspicIros18,
  title={{ASPiC: an Acting system based on Skill Petri net Composition}},
  author={Charles Lesire and Franck Pommereau},
  booktitle={International Conference on Intelligent Robots and Systems (IROS)},
  address={Madrid, Spain},
  year=2018
}
```

The slides of the 3min [lightning talk](/doc/aspic3min.pdf) and the
[interactive](/doc/aspicInteractivePoster.pdf) session are also available.

## Install instructions

### Dependencies

ASPiC needs the following Python dependencies: *enum34* and *snakes*. To install them:
```
sudo pip install SNAKES
sudo pip install enum34
```

### Using ROS

Create a workspace and clone the repository in the workspace sources. Then build with catkin. When you source the setup script, ASPiC is in your python path.

```
mkdir -p aspic_ws/src
cd aspic_ws
git clone https://gitlab.com/charles-lesire/aspic src/aspic
catkin_make
source devel/setup.bash
python -c 'import aspic'
```

### Using Python

The ASPiC sources can be directly installed in your python folders using the setuptools.

```
git clone https://gitlab.com/charles-lesire/aspic
cd aspic
sudo python setup.py --install
python -c 'import aspic'
```

# Basic Tutorial

This tutorial shows how to use ASPiC to create a simple action supervisor. We will
first build a simple action and executes it.

The final script is available in the basic [tutorial](/tutorial/tutorial.py).

## The action

We assume the "robot" we want to control provides a *Fibonacci* skill, that computes
the Fibonacci sequence up to the *n*-th iteration. The supervisor action is
to first trigger the Fibonacci skill, get the result, and test whether the greatest
number of the returned sequence has reached a *target*. If yes, the action
succeeds. Otherwise, we ask the skill to compute one more iteration.

## The Fibonacci skill

We first create a *SkillPetriNet* named *Sfib* that takes as input the state *n*,
that contains the number of iterations we require, and that returns as outputs
the states *sequence*, that will contain the complete Fibonacci sequence, and *value*
that will contain the greatest value of this sequence.

```python
from aspic import *

Sfib = SkillPetriNet("Sfib", inputs=['n'], outputs=['sequence', 'value'])
```

Draw the *Sfib* net to a file:
```python
draw(Sfib, "Sfib.pdf")
```

If you launch this script, you will get the following pdf file:
<p align="center">
  <img src="/tutorial/Sfib.png">
</p>

In this figure, you can notice:
* the control-flow places in gray
* the inputs and outputs as *buffer* places

## Test the Fibonacci output

Now we will create an internal skill that tests the value returned by the Fibonacci skill. In that purpose, ASPiC provides a *TestHandler* that tests the inputs of the skill according to an acceptance function:

```python
Stest = SkillPetriNet("Stest", inputs=['value', 'target'])
Htest = TestHandler("Htest", fun=lambda x: x[0] >= x[1])
Ntest = handle(Stest, Htest)
draw(Ntest, "Ntest.pdf")

N = sequence(Sfib, Ntest)
draw(N, "N.pdf")
```

This python code first creates a skill named *Stest*, that takes as input the *value* state, and
the *target* state. Then the handler *Htest* tests whether the value is greater or equals to the target. Note that the values of the states given as input as available in the handler as
a list, and must then be accessed through their index.
The script finally connects the skill and the handler, and draw the result as:

<p align="center">
  <img src="/tutorial/Ntest.png">
</p>

Then the script defines *N* as the sequence of the Fibonacci skill and the test.
Look at the resulting pdf: the common buffer *value* as been merged, and the exit place of *Sfib* has been merged with the entry place of *Ntest*.

## Increment the iterations in case of failure

### Increment *n*

If case the Fibonacci result is not accepted, we want to make a new request on
the Fibonacci skill with one more iteration. We then need to increment the value *n*:

```python
Sinc = SkillPetriNet("Sinc", inputs=['n'], outputs=['n'])
Hinc = AssignHandler("Hinc", fun=hret(lambda x: {'n': x[0] + 1}))
Ninc = handle(Sinc, Hinc)
draw(Ninc, "Ninc.pdf")
```

*Sinc* defines the skill for incrementing *n*. It takes *n* as input and as output.
To increment the value of *n*, we use the handle *AssignHandler* provided by ASPiC.
This handler always succeeds, and computes the output value according to a given function.
The function used for the *Hinc* handler increments the value of *n*. Note that:
 * the input value of *n* is accessed as `x[0]`, as for the test handler
 * the output of an handler must be a *hashable* dict, with the keys being the name of the outputs, and the value the new value to assign.
Actually, all tokens must be hashable objects. In that purpose, ASPiC provides the `hret` decorator
that converts a function output into a hashable object (when possible).

The script then draws the combination of the skill and the handler.

<p align="center">
  <img src="/tutorial/Ninc.png">
</p>

### Increment in case of failure

Skills can only be started by success tokens.
Moreover, the *Ninc* net will systematically succeed as the handler cannot fail.
What we want to do, is to trigger *Ninc* only in case the previous test has failed.
Moreover, the result of the combination must succeed only if the test has succeeded.
The solution is then to apply a double negation:

```python
N = sequence(negation(N), Ninc)
N = negation(N)
draw(N, "N.pdf")
```

## Loop

Now we have incremented the value of *n*, we just have to retry to call the Fibonacci
skill with the new value:

```python
N = retry(N)
draw(N, "N.pdf")
```

## Initialize and launch the action

In order to execute the action, we must bind a handler to the Fibonacci skill.
An handler must be attached to an elementary skill, and then cannot be directly attached to net *N*

After the code defining *Sfib*, add the following code:

```python
fib_sequence = [0,1,1,2,3,5,8]

@hret
def fibonacci(inputs):
    n = inputs[0]
    return {'sequence': fib_sequence[:n],
            'value': fib_sequence[n]}

Hfib = AssignHandler("Hfib", fun=fibonacci)
Nfib = handle(Sfib, Hfib)
```

This handler "simulates" a correct behavior of the Fibonacci skill by using the *AssignHandler*.

Then replace *Sfib* by *Nfib* in the rest of the script.

To initialize the final Petri net, ASPiC provides a function that takes the initial values
of buffers, i.e. of states and locks, as an input:

```python
initialize(N, {
    'n': 1,
    'value': -1,
    'sequence': -1,
    'target': 4,
    })
draw(N, "N.pdf")
```

Take care that all states must initially own a value. In this example we have added
value `-1` to represent an *undefined* value. You can also define your own value/constant
to initialize them.
The initialize function also puts a success token in the entry place of the net.

In order to execute the net, add a `while` loop that makes steps until success or failure:

```python
while True:
    step(N, verbose=False, once=True)
    draw(N, "N.pdf")
    if has_succeeded(N):
        print("{} has succeeded".format(N))
        break
    elif has_failed(N):
        print("{} has failed".format(N))
        break

M = N.get_marking()
v = N.status(buffer('value'))[0]
print("Result: {}".format(M[v]))
n = N.status(buffer('n'))[0]
print("Needed iterations: {}".format(M[n]))
```

Then run the script: to get a value greater than 4, you should have made 5 iterations and get the number 5 as result.

# ROS Tutorial

In this tutorial, we will connect the Fibonacci skill to a ROS action server implementing the skill.

The final script is available in the ROS [tutorial](/tutorial/tutorial_ros.py).

## Import ROS

You first need to install the ROS Actionlib Tutorial:
```
sudo apt install ros-kinetic-actionlib-tutorials
```
This tutorial provides an action Fibonacci server that computes the Fibonacci sequence
up to the iterations passed as a *goal*.

In your script, in addition to *aspic*, you must also import *aspic.ros*, that provides
an handler to connect to ROS actions. You must also import *rospy* in order to
initialize the node, and the message types of the Fibonacci server.

```python
from aspic import *
from aspic.ros import *
import rospy
from actionlib_tutorials.msg import FibonacciAction, FibonacciGoal
```

## ROS Actionlib Handler

ASPiC provides a class wrapping the ROS Actionlib client, that is used in
the handler Petri net:

```python
Sfib = SkillPetriNet("Sfib", inputs=['n'], outputs=['sequence', 'value'])

@hret
def format_result(result):
    fibonacci_sequence = result.sequence
    return {'sequence': fibonacci_sequence,
            'value': fibonacci_sequence[-1]}

fib_client = RosActionlibClient('fibonacci', FibonacciAction,
                        goal_fmt=lambda x: FibonacciGoal(order=x[0]),
                        result_fmt=format_result)
Nfib = handle(Sfib, RosActionlibHandler(fib_client))
```

The `RosActionlibClient` takes as arguments: the name of the server, the type
of action, a function that formats the action goal from skill inputs, and
a function that formats the results as a hashable dictionnaty of outputs.

This client is then used to initialize the handler Petri net.

## Operators

The sequel of the script that builds the ASPiC Petri net is similar to the basic tutorial:

```python
Stest = SkillPetriNet("Stest", inputs=['value', 'target'])
Htest = TestHandler("Htest", fun=lambda x: x[0] >= x[1])
Ntest = handle(Stest, Htest)
N = sequence(Nfib, Ntest)

Sinc = SkillPetriNet("Sinc", inputs=['n'], outputs=['n'])
Hinc = AssignHandler("Hinc", fun=hret(lambda x: {'n': x[0] + 1}))
Ninc = handle(Sinc, Hinc)

N = sequence(negation(N), Ninc)
N = negation(N)

N = retry(N)

initialize(N, {
    'n': 1,
    'value': -1,
    'sequence': 0,
    'target': 4,
    })

draw(N, "N.pdf")
```

## Initialize the node and execute the Petri net

In this script, we define a ROS node named *aspic*, that runs at a frequency
of 1 Hz. We then wait for the Fibonacci server to be ready.

```python
rospy.init_node("aspic")

rate = rospy.Rate(1)
fib_client.wait_for_server()

while not rospy.is_shutdown():
    step(N, verbose=False, once=True)
    draw(N, "N.pdf")
    if has_succeeded(N):
        print("{} has succeeded".format(N))
        break
    elif has_failed(N):
        print("{} has failed".format(N))
        break
    rate.sleep()
```

The *while* loop then does calls the same function as in the basic tutorial, except
that we wait for the given period at each iteration.

The end of the script is similar to the basic tutorial:
```python
M = N.get_marking()
v = N.status(buffer('value'))[0]
print("Result: {}".format(M[v]))
n = N.status(buffer('n'))[0]
print("Needed iterations: {}".format(M[n]))
```

## Launch the server and ASPiC

In order to launch the tutorial, you first need to:
* start a *roscore*
* launch the server using `rosrun actionlib_tutorials fibonacci_server`
* launch your ASPiC script using `python`
