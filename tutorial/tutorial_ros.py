#!/usr/bin/env python
#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from aspic import *
from aspic.ros import *
import rospy
from actionlib_tutorials.msg import FibonacciAction, FibonacciGoal

# Exercice 1: fibonacci skill
Sfib = SkillPetriNet("Sfib", inputs=['n'], outputs=['sequence', 'value'])

@hret
def format_result(result):
    fibonacci_sequence = result.sequence
    return {'sequence': fibonacci_sequence,
            'value': fibonacci_sequence[-1]}

fib_client = RosActionlibClient('fibonacci', FibonacciAction,
                        goal_fmt=lambda x: FibonacciGoal(order=x[0]),
                        result_fmt=format_result)
Nfib = handle(Sfib, RosActionlibHandler(fib_client))
draw(Nfib, "Nfib.png")

# Exercice 2: test handler + sequence
Stest = SkillPetriNet("Stest", inputs=['value', 'target'])
Htest = TestHandler("Htest", fun=lambda x: x[0] >= x[1])
Ntest = handle(Stest, Htest)
draw(Ntest, "Ntest.pdf")

N = sequence(Nfib, Ntest)
draw(N, "N.pdf")

# Exercice 3: increment 'n' if fdot
Sinc = SkillPetriNet("Sinc", inputs=['n'], outputs=['n'])
Hinc = AssignHandler("Hinc", fun=hret(lambda x: {'n': x[0] + 1}))
Ninc = handle(Sinc, Hinc)
draw(Ninc, "Ninc.pdf")

# Exercice 4: increment 'n' if fdot
N = sequence(negation(N), Ninc)
N = negation(N)
draw(N, "N.pdf")

# Exercice 5: loop until test
N = retry(N)
draw(N, "N.pdf")

initialize(N, {
    'n': 1,
    'value': -1,
    'sequence': 0,
    'target': 4,
    })

draw(N, "N.pdf")

rospy.init_node("aspic")

rate = rospy.Rate(1)
fib_client.wait_for_server()

while not rospy.is_shutdown():
    step(N, verbose=False, once=True)
    draw(N, "N.pdf")
    if has_succeeded(N):
        print("{} has succeeded".format(N))
        break
    elif has_failed(N):
        print("{} has failed".format(N))
        break
    rate.sleep()

M = N.get_marking()
v = N.status(buffer('value'))[0]
print("Result: {}".format(M[v]))
n = N.status(buffer('n'))[0]
print("Needed iterations: {}".format(M[n]))
