#!/usr/bin/env python
#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from aspic import *

# Exercice 1: fibonacci skill
Sfib = SkillPetriNet("Sfib", inputs=['n'], outputs=['sequence', 'value'])
Hfib = SimulationHandler("Hfib",
    f_success=hret(lambda x: {'sequence': [], 'value': 8}),
    f_failure=hret(lambda x: {'sequence': [], 'value': -1}))
Nfib = handle(Sfib, Hfib)
draw(Nfib, "Nfib.pdf")

# Exercice 2: test handler + sequence
Stest = SkillPetriNet("Stest", inputs=['value', 'target'])
Htest = TestHandler("Htest", fun=lambda x: x[0] >= x[1])
Ntest = handle(Stest, Htest)
draw(Ntest, "Ntest.pdf")

N = sequence(Nfib, Ntest)
draw(N, "N.pdf")

# Exercice 3: increment 'n' if fdot
Sinc = SkillPetriNet("Sinc", inputs=['n'], outputs=['n'])
Hinc = AssignHandler("Hinc", fun=hret(lambda x: {'n': x[0]}))
Ninc = handle(Sinc, Hinc)
draw(Ninc, "Ninc.pdf")

# Exercice 4: increment 'n' if fdot
N = sequence(negation(N), Ninc)
N = negation(N)
draw(N, "N.pdf")

# Exercice 5: loop until test
N = retry(N)
draw(N, "N.pdf")

initialize(N, {
    'n': 1,
    'value': -1,
    'sequence': 0,
    'target': 1,
    })

draw(N, "N.pdf")

def draw_sg(i, sg, attrs):
    m = sg[i]
    cfm = ["{} -> {}".format(p.name, m(p.name)) for p in N.place() if p.status in [entry,exit,internal] and m(p.name)]
    attrs["label"] = cfm

    if has_succeeded(N, m):
        attrs['color'] = 'blue'
        attrs['peripheries'] = 2
        attrs['fillcolor'] = 'azure2'
    elif has_failed(N, m):
        attrs['color'] = 'red'
        attrs['peripheries'] = 2
        attrs['fillcolor'] = 'lightred'

sg = StateGraph(N)
sg.build()
sg.draw("StateGraph.pdf", node_attr=draw_sg)
